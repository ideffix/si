<?php
class Person {
    public $name;
    public $surname;
    public $gender;
    public $email;
    public $zipCode;

    public function __construct($name, $surname, $gender, $email, $zipCode) {
        $this->name = $name;
        $this->surname = $surname;
        $this->gender = $gender;
        $this->email = $email;
        $this->zipCode = $zipCode;
    }

    public function getFullData() {
        return 'Imię: '.$this->name.'<br />'.
        'Nazwisko: '.$this->surname.'<br />'.
        'Płeć: '.$this->gender.'<br />'.
        'Email: '.$this->email.'<br />'.
        'Kod pocztowy: '.$this->zipCode;
    }
}
?>