<?php
$formResult = '';
if (isset($_SESSION['error'])) {
    $formResult = $_SESSION['error'];
    unset($_SESSION['error']);
} else if (isset($_SESSION['success'])) {
    $formResult = $_SESSION['success'];
    unset($_SESSION['success']);
}
$middleContent = '
<form class="form-style" action="sendForm.php" method="POST">
    <div>
        <div class="input-desc">Imię:</div> <input class="inputs-width" type="text" name="name" />
    </div>
    <div>
        <div class="input-desc">Nazwisko:</div> <input class="inputs-width" type="text" name="surname" />
    </div>
    <div>
        <div class="input-desc">Płeć:</div>
        <div class="inline-block radio-width">
            <input type="radio" name="gender" value="Mężczyzna">Mężczyzna</input>
            <input type="radio" name="gender" value="Kobieta">Kobieta</input>
        </div>
    </div>
    <div>
    	<div class="input-desc">Email:</div> <input class="inputs-width" type="text" name="email" />
    </div>
    <div>
        <div class="input-desc">Kod pocztowy:</div> <input class="inputs-width" type="text" name="zipCode" />
    </div>
    <div>
        <input class="submit-margin" type="submit" value="Wyślij" />
    </div>'.$formResult.'
</form>
'
?>