<?php 
session_start();
if (isset($_GET['site'])) {
	if ($_GET['site'] == 'mainPage') {
		include('mainPage.php');
	} else if ($_GET['site'] == 'formPage') {
		include('formPage.php');
	} else if ($_GET['site'] == 'sessionPage') {
		include('sessionPage.php');
	} else if ($_GET['site'] == 'employees') {
		include('employeesPage.php');
	}
} else {
	include('mainPage.php');
}

include('footer.php');

echo '<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Systemy Internetowe</title>

	<meta name="description" content="Projekt na zaliczenie przedmiotu systemy internetowe." />
	<meta name="keywords" content="systemy internetowe, systemy, zaliczonko, wow, dobre oceny, seo, pocokomubutstrap" />

	<link href="css/style.css" type="text/css" rel="stylesheet">
	<script src="js/script.js" ></script>

</head>
<body>
	<div class="header">
		<h1>Header</h1>
	</div>
	<div class="content">
		<div class="left common side">
			<ol>
				<li>
					<a href="index.php?site=mainPage">
						Strona główna
					</a>
				</li>
				<li>
					<a href="index.php?site=formPage">
						Formularz
					</a>
				</li>
				<li>
					<a href="index.php?site=sessionPage">
						Zawartość sesji
					</a>
				</li>
				<li>
					<a href="index.php?site=employees">
						Baza pracowników
					</a>
				</li>
					
			</ol>
		</div>
		<div class="middle common">
		';
		echo "$middleContent";
		echo 
		'
		</div>
		<div class="right common side">
			<form method="get" action="index.php">
 				<input type="text" name="criteria" />
				<input type="hidden" name="site" value="employees" />
 				<input type="submit" value="Szukaj" />
			</form>
		</div>
	</div>
	<div class="footer">
		<h4>';
		echo "$footerContent";
		echo '</h4>
	</div>

</body>
</html>';
?>
