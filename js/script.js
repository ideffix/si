function changeGoogleColor() {
    var coloredGoogle = "<span style=\"color: #3369E8\" !important>G</span>" +
        "<span style=\"color: #D50F25\" >o</span>" +
        "<span style=\"color: #EEB211\" >o</span>" +
        "<span style=\"color: #3369E8\" >g</span>" +
        "<span style=\"color: #009925\" >l</span>" +
        "<span style=\"color: #D50F25\" >e</span>";
    var google = document.getElementById("google");
    google.innerHTML = coloredGoogle;   
}

function setNormalGoogleColor() {
    var google = document.getElementById("google");
    google.innerHTML = "Google";
}