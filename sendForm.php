<?php
error_reporting(E_ALL);
session_start();
unset($_SESSION['error']);
header('Location: index.php?site=formPage');

if (validate()) {
    handleCorrectInputData();
} else {
    exit();
}

function handleCorrectInputData() {
    $_SESSION['success'] = '<span class="success">Klient dodany!</span>';
    require('Person.php');
    $janusz = new Person($_POST['name'], $_POST['surname'], $_POST['gender'], $_POST['email'], $_POST['zipCode']);
    addPersonToSession($janusz);
    addPersonToDB($janusz);
    header('Location: index.php?site=sessionPage');
    exit();
}

function checkIfUserFilledAllFields() {
    $valid = true;
    if (!isset($_POST['name']) || !isset($_POST['surname']) || !isset($_POST['gender']) || 
    !isset($_POST['email']) || !isset($_POST['zipCode'])) {
        $_SESSION['error'] = '<span class="error">Brak wszystkich danych</span>';
        $valid = false;
    }
    return $valid;
}

function validEmail($pattern) {
    $valid = true;
    if (!preg_match($pattern, $_POST['email'])) {
        $_SESSION['error'] = '<span class="error">Błędny adres email</span>';
        $valid = false;
    }
    return $valid;
}

function validZipCode($pattern) {
    $valid = true;
    if (!preg_match($pattern, $_POST['zipCode'])) {
        $_SESSION['error'] = '<span class="error">Błędny kod pocztowy</span>';
        $valid = false;
    }
    return $valid;
}

function validate() {
    $zipCodeRegex = '/\d{2}-\d{3}/';
    $emailRegex = '/^\S+@\S+$/'; 
    return checkIfUserFilledAllFields() && validEmail($emailRegex) && validZipCode($zipCodeRegex);
}

function addPersonToSession($person) {
    if (isset($_SESSION['people'])) {
        $people = $_SESSION['people'];
        $people[] = serialize($person);
    } else {
        $people = array(serialize($person));
    }
    $_SESSION['people'] = $people;
}

function addPersonToDB($person) {
    $connection = getDBConnection();
    if ($connection->connect_errno != 0) {
		echo "Error: ".$connection->connect_errno;
	} else {
        $connection->query("INSERT INTO users VALUES (NULL, '$person->name', '$person->surname', '$person->gender', '$person->email', '$person->zipCode')");
    }
    $connection->close();
}

function getDBConnection() {
    require_once('databaseConnection.php');
    return @new mysqli($host, $db_user, $db_password, $db_name);
}


?>

